/**
 * File: /package.json
 * Project: keycloak-get-admin-cli
 * File Created: 29-08-2021 11:13:07
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 30-08-2021 15:03:23
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// <reference types="cypress" />

import cypressDotenv from 'cypress-dotenv';
import webpackPreprocessor from '@cypress/webpack-preprocessor';
import { Configuration as WebpackConfiguration } from 'webpack';
import { RuleSetUseItem } from 'webpack';

const defaults = webpackPreprocessor.defaultOptions;
const babelConfig: BabelConfig = {
  plugins: []
};
const defaultBabelConfig: BabelConfig = getDefaultBabelConfig(defaults);

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (_on: any, config: any) => {
  const mergedBabelConfig = mergeBabelConfig(babelConfig);
  Object.assign(defaultBabelConfig, mergedBabelConfig);
  // on('file:preprocessor', webpackPreprocessor(defaults));
  config = cypressDotenv(config, {}, true);
  return config;
};

export function getDefaultBabelConfig({
  webpackOptions
}: DefaultOptions): BabelConfig {
  return (
    (
      (webpackOptions?.module?.rules?.[0] as { use: RuleSetUseItem[] })
        .use?.[0] as { options: Record<string, any> }
    )?.options || {}
  );
}

export function mergeBabelConfig(babelConfig: BabelConfig) {
  return {
    ...defaultBabelConfig,
    ...babelConfig,
    presets: [
      ...(defaultBabelConfig.presets || []),
      ...(babelConfig.presets || [])
    ],
    plugins: [
      ...(defaultBabelConfig.plugins || []),
      ...(babelConfig.plugins || [])
    ]
  };
}

export interface BabelConfig {
  plugins?: string[];
  presets?: string[];
  [key: string]: any;
}

interface DefaultOptions {
  webpackOptions?: WebpackConfiguration;
  watchOptions?: Object;
  typescript?: string;
}

module.exports(() => {});
